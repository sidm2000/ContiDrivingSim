package com.conti_online.continentaldrivingsimulation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.graphics.Color;
import android.util.Log;
import android.view.Display;
import android.view.View;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private URL url;
    private String curr="Text:", curr2;
    private boolean multiply;
    private int ADD_SHIFT, place=0;
    private double MULT_SHIFT;
    private JSONParser jp=new JSONParser();

    public static ArrayList<Integer> intersectionX = new ArrayList<Integer>(), intersectionY = new ArrayList<Integer>(), numInters = new ArrayList<Integer>(), interIds = new ArrayList<Integer>();
    public static ArrayList<Integer> segmentId = new ArrayList<Integer>(), inter1 = new ArrayList<Integer>(), inter2 = new ArrayList<Integer>();
    public static ArrayList<Integer> carStuck = new ArrayList<Integer>(), carColor = new ArrayList<Integer>(), carSpeed = new ArrayList<Integer>(), carDir = new ArrayList<Integer>(),carDir2 = new ArrayList<Integer>(),  carIds = new ArrayList<Integer>(), carSeg = new ArrayList<Integer>(), carPos = new ArrayList<Integer>(), carXPos = new ArrayList<Integer>(), carYPos = new ArrayList<Integer>();
    public static ArrayList<Integer> sigId = new ArrayList<Integer>(), intId = new ArrayList<Integer>(), lightSegId = new ArrayList<Integer>(), lightStates = new ArrayList<Integer>();
    public static DrawView drawView;
    public static int segId=0, id, state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ZoomView2 zoomView2;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED){
            drawView = new DrawView(this, intersectionX, intersectionY, 0);
            Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            if(hour<4 || hour>18) drawView.setBackgroundColor(getResources().getColor(R.color.darkBackground));
            Thread thread = new Thread(new Runnable(){
                @Override
                public void run(){
                    try{
                        url = new URL("http://192.168.0.11:8080/static/");
                        //url=new URL("https://srv-file1.gofile.io/download/KQb8tL/e367c58ca749a01aef00e504ab0bf7ff/static.txt");
                        URLConnection urlc = url.openConnection();
                        InputStream is = urlc.getInputStream();
                        BufferedReader in = new BufferedReader(new InputStreamReader(is));
                        String inputLine;
                        while((inputLine=in.readLine())!=null) curr+=inputLine;
                        Log.i("TEXT IN THREAD", curr);
                        jp.parseInterIds(curr);
                        int max=Integer.MIN_VALUE;
                        for(int val:intersectionX) if(val>max) max=val;
                        for(int val:intersectionY) if(val>max) max=val;
                        ADD_SHIFT = max+500;
                        Display mdisp = getWindowManager().getDefaultDisplay();
                        if(max>mdisp.getWidth()){
                            multiply=false;
                            MULT_SHIFT=max/(mdisp.getWidth()/4);
                        }else{
                            multiply=true;
                            MULT_SHIFT=mdisp.getWidth()/(max/4);
                        }
                        if(multiply){
                            for(int x=0; x<intersectionX.size(); x++){
                                intersectionX.set(x, (int)(intersectionX.get(x)+ADD_SHIFT*MULT_SHIFT));
                                intersectionY.set(x, (int)(intersectionY.get(x)+ADD_SHIFT*MULT_SHIFT));
                            }
                        }else{
                            for(int x=0; x<intersectionX.size(); x++){
                                intersectionX.set(x, (int)(intersectionX.get(x)+ADD_SHIFT/MULT_SHIFT));
                                intersectionY.set(x, (int)(intersectionY.get(x)+ADD_SHIFT/MULT_SHIFT));
                            }
                        }
                        jp.parseSegments(curr);
                        jp.parseSignals(curr);
                        //TODO implement JSON library
                    }catch(Exception j){Log.e("STATIC ERROR",j.toString());}
                }
            });
            thread.start();
            zoomView2 = new ZoomView2(getApplicationContext());
            while(thread.isAlive()){}
            for(int x=0; x<intersectionX.size(); x++) numInters.add(0);
            for(int val=0; val<inter1.size(); val++){
                numInters.set(inter1.get(val), numInters.get(inter1.get(val))+1);
                numInters.set(inter2.get(val), numInters.get(inter2.get(val))+1);
            }
            zoomView2.addView(drawView);
            zoomView2.setMaxZoom(400);
            setContentView(zoomView2);
            //Log.d("SEGMENT ID"+segmentId.size()+":"+segId, segmentId.toString());
            //Log.d("SEGMENT A"+inter1.size(), inter1.toString());
            //Log.d("SEGMENT B"+inter2.size(), inter2.toString());
            //Log.e("INTERSECTION X"+intersectionX.size(), intersectionX.toString());
            //Log.e("INTERSECTION Y"+intersectionY.size(), intersectionY.toString());
            Log.e("INTERSECTION ID"+interIds.size(), interIds.toString());
            //Log.d("SIGNALS", segmentId.get(111)+" "+inter1.get(111)+" "+inter2.get(111));
            //Log.d("SIGNAL ID"+sigId.size(), sigId.toString());
            Log.e("SIGNAL INTERSECTION"+intId.size(), intId.toString());
            //Log.d("SIGNAL SEGMENT"+lightSegId.size(), lightSegId.toString());
            getDynamicInfo();
        }else{
            new android.app.AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("You must have a WiFi connection for this app to function.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .setIcon(R.drawable.alert_icon)
                    .show();
        }
    }

    private void getDynamicInfo(){
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    /**
                     *  Hard-coded car movement
                     */
                    for(int x=0; x<intersectionX.size(); x++){
                        carIds.add(x);
                        int index;
                        do{
                            index=(int)(Math.random()*intersectionX.size());
                        }while(numInters.get(index)!=4);
                        carXPos.add(intersectionX.get(index));
                        carYPos.add(intersectionY.get(index));
                        carSpeed.add(1);
                        carDir2.add((int)(Math.random()*4));
                        carColor.add(Color.rgb((int)(Math.random()*255), (int)(Math.random()*255), (int)(Math.random()*255)));
                        carStuck.add(0);
                    }
                    /***/
                    Socket clientSocket = new Socket("192.168.0.11", 8081);
                    BufferedReader in2 = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    char[] buffer = new char[2048];
                    int charsRead;
                    while((charsRead=in2.read(buffer)) != -1)
                    {
                        place=1;
                        curr2 = new String(buffer).substring(0, charsRead);
                        Log.i("TEXT IN THREAD", curr2);
                        if(curr2.contains("}}")/* && curr2.indexOf('{')==0*/){
                            place=2;
                            if(curr2.contains("SignalID")){
                                jp.parseDynSignals(curr2);
                                lightStates.set(id, state);
                                place=3;
                            }
                            /**
                             *  More hard-code
                             */
                            else if(curr2.contains("Speed") && carIds.contains(Integer.parseInt(curr2.substring(curr2.indexOf("CarID")+7, curr2.indexOf(',', curr2.indexOf("CarID")))))){
                                carSpeed.set(Integer.parseInt(curr2.substring(curr2.indexOf("CarID")+7, curr2.indexOf(',', curr2.indexOf("CarID")))),
                                        Integer.parseInt(curr2.substring(curr2.indexOf("Speed")+7, curr2.indexOf(',', curr2.indexOf("Speed"))))/2);
                            }
                            /**
                             *  Automatic car movement
                             */
                            /*else{
                                place=4;
                                if(!carIds.contains(Integer.parseInt(curr2.substring(curr2.indexOf("CarID")+7, curr2.indexOf(',', curr2.indexOf("CarID"))))) && !curr2.contains("CarIndState")){//if new car found
                                    jp.parseCars(curr2);
                                    place=5;
                                    int segPartOne=0, segPartTwo=0, startIntX=0, startIntY=0, endIntX=0, endIntY=0, addition;
                                    int tempId = carSeg.get(numCars);
                                    for(int x=0; x<segmentId.size(); x++){
                                        place=6;
                                        if(segmentId.get(x)==tempId){
                                            segPartOne=inter1.get(x);
                                            segPartTwo=inter2.get(x);
                                            place=7;
                                            break;
                                        }
                                    }
                                    if(segPartOne<segPartTwo){
                                        place=8;
                                        startIntX=intersectionX.get(segPartOne);
                                        startIntY=intersectionY.get(segPartOne);
                                        endIntX=intersectionX.get(segPartTwo);
                                        endIntY=intersectionY.get(segPartTwo);
                                    }else if(segPartTwo<segPartOne){
                                        place=9;
                                        startIntX=intersectionX.get(segPartTwo);
                                        startIntY=intersectionY.get(segPartTwo);
                                        endIntX=intersectionX.get(segPartOne);
                                        endIntY=intersectionY.get(segPartOne);
                                    }
                                    if(carDir.get(numCars)==0){
                                        place=10;
                                        if(startIntX<endIntX && startIntY==endIntY) carDir2.add(0);
                                        else if(startIntX<endIntX && startIntY>endIntY) carDir2.add(4);
                                        else if(startIntY>endIntY && startIntX==endIntX) carDir2.add(1);
                                        else if(startIntX>endIntX && startIntY>endIntY) carDir2.add(5);
                                        else if(startIntX>endIntX && startIntY==endIntY) carDir2.add(2);
                                        else if(startIntX>endIntX && startIntY<endIntY) carDir2.add(6);
                                        else if(startIntY<endIntY && startIntX==endIntX) carDir2.add(3);
                                        else if(startIntX<endIntX && startIntY<endIntY) carDir2.add(7);
                                        else carDir2.add(-1);//for diagonal segments
                                    }else{
                                        place=11;
                                        if(startIntX<endIntX && startIntY==endIntY) carDir2.add(2);
                                        else if(startIntX<endIntX && startIntY>endIntY) carDir2.add(6);
                                        else if(startIntY>endIntY && startIntX==endIntX) carDir2.add(3);
                                        else if(startIntX>endIntX && startIntY>endIntY) carDir2.add(7);
                                        else if(startIntX>endIntX && startIntY==endIntY) carDir2.add(0);
                                        else if(startIntX>endIntX && startIntY<endIntY) carDir2.add(4);
                                        else if(startIntY<endIntY && startIntX==endIntX) carDir2.add(1);
                                        else if(startIntX<endIntX && startIntY<endIntY) carDir2.add(5);
                                        else carDir2.add(-1);
                                    }
                                    addition=carPos.get(numCars)/20;
                                    place=12;
                                    if(carDir2.get(numCars)==0){
                                        carXPos.add(startIntX+addition);
                                        carYPos.add(startIntY);
                                    }else if(carDir2.get(numCars)==1){
                                        carXPos.add(startIntX);
                                        carYPos.add(startIntY-addition);
                                    }else if(carDir2.get(numCars)==2){
                                        carXPos.add(startIntX-addition);
                                        carYPos.add(startIntY);
                                    }else if(carDir2.get(numCars)==3){
                                        carXPos.add(startIntX);
                                        carYPos.add(startIntY+addition);
                                    }
                                    numCars++;
                                }else if(!curr2.contains("CarIndState")){//update existing car's status
                                    place=13;
                                    int tempId=jp.parseCarId(curr2);
                                    for(int z=0; z<carIds.size(); z++){
                                        place=14;
                                        if(tempId==carIds.get(z)){
                                            place=15;
                                            jp.parseDynCars(curr2, z);
                                            int segPartOne=0, segPartTwo=0, startIntX=0, startIntY=0, endIntX=0, endIntY=0;
                                            for(int x=0; x<segmentId.size(); x++){
                                                if(segmentId.get(x)==tempId){
                                                    segPartOne=inter1.get(x);
                                                    segPartTwo=inter2.get(x);
                                                    place=16;
                                                    break;
                                                }
                                            }
                                            if(segPartOne<segPartTwo){
                                                place=17;
                                                startIntX=intersectionX.get(segPartOne);
                                                startIntY=intersectionY.get(segPartOne);
                                                endIntX=intersectionX.get(segPartTwo);
                                                endIntY=intersectionY.get(segPartTwo);
                                            }else if(segPartOne>segPartTwo){
                                                place=18;
                                                startIntX=intersectionX.get(segPartTwo);
                                                startIntY=intersectionY.get(segPartTwo);
                                                endIntX=intersectionX.get(segPartOne);
                                                endIntY=intersectionY.get(segPartOne);
                                            }
                                            int numSegs;
                                            for(int y=0; y<numInters.size(); y++){
                                                place=19;
                                                if(intersectionX.get(y)<carXPos.get(z)+100 && intersectionX.get(y)>carXPos.get(z)-100 && intersectionY.get(y)<carYPos.get(z)+100 && intersectionY.get(y)>carYPos.get(z)-100) {
                                                    place=20;
                                                    numSegs = numInters.get(y);
                                                    if(numSegs==4){
                                                        place=21;
                                                        if(carDir.get(z)==0){
                                                            place=22;
                                                            if(startIntX<endIntX && startIntY==endIntY) carDir2.set(z, 0);//right
                                                            else if(startIntX<endIntX && startIntY>endIntY) carDir2.set(z, 4);//top-right
                                                            else if(startIntY>endIntY && startIntX==endIntX) carDir2.set(z, 1);//up
                                                            else if(startIntX>endIntX && startIntY>endIntY) carDir2.set(z, 5);//top-left
                                                            else if(startIntX>endIntX && startIntY==endIntY) carDir2.set(z, 2);//left
                                                            else if(startIntX>endIntX && startIntY<endIntY) carDir2.set(z, 6);//bottom-left
                                                            else if(startIntY<endIntY && startIntX==endIntX) carDir2.set(z, 3);//down
                                                            else if(startIntX<endIntX && startIntY<endIntY) carDir2.set(z, 7);//bottom-right
                                                            else carDir2.set(z, -1);//for unusual segments
                                                        }else{
                                                            place=23;
                                                            if(startIntX<endIntX && startIntY==endIntY) carDir2.set(z, 2);
                                                            else if(startIntX<endIntX && startIntY>endIntY) carDir2.set(z, 6);
                                                            else if(startIntY>endIntY && startIntX==endIntX) carDir2.set(z, 3);
                                                            else if(startIntX>endIntX && startIntY>endIntY) carDir2.set(z, 7);
                                                            else if(startIntX>endIntX && startIntY==endIntY) carDir2.set(z, 0);
                                                            else if(startIntX>endIntX && startIntY<endIntY) carDir2.set(z, 4);
                                                            else if(startIntY<endIntY && startIntX==endIntX) carDir2.set(z, 1);
                                                            else if(startIntX<endIntX && startIntY<endIntY) carDir2.set(z, 5);
                                                            else carDir2.set(z, -1);
                                                        }
                                                    }else{
                                                        place=24;
                                                        if(carDir2.get(z)==0) carDir2.set(z, 2);
                                                        else if(carDir2.get(z)==1) carDir2.set(z, 3);
                                                        else if(carDir2.get(z)==2) carDir2.set(z, 0);
                                                        else if(carDir2.get(z)==3) carDir2.set(z, 1);
                                                    }
                                                    break;
                                                }
                                            }
                                            switch(lightStates.get(???)){
                                                case 0:
                                                    carSpeed.set(z, 0);
                                                    break;
                                                case 1:
                                                    carSpeed.set(z, 1);
                                                    break;
                                                case 2:
                                                    if(carSpeed.get(z)<2) carSpeed.set(z, 2);
                                                    break;
                                                default: break;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }*/
                        }
                        for(int z=0; z<carIds.size()-1; z++){//move car
                            place=25;
                            int direction=carDir2.get(z), speed=carSpeed.get(z), x=carXPos.get(z), y=carYPos.get(z), numSegs;
                            /*if(carStuck.get(z)==1){
                                speed=1;
                                carStuck.set(z, 0);
                            }*/
                            /*switch(direction){
                                case 0:
                                    if(!carXPos.contains(x+speed)) carXPos.set(z, x+speed);
                                    else{
                                        carStuck.set(z, 1);
                                        carSpeed.set(z, 0);
                                    }
                                    break;
                                case 1:
                                    if(!carYPos.contains(y-speed)) carYPos.set(z, y-speed);
                                    else{
                                        carStuck.set(z, 1);
                                        carSpeed.set(z, 0);
                                    }
                                    break;
                                case 2:
                                    if(!carXPos.contains(x-speed)) carXPos.set(z, x-speed);
                                    else{
                                        carStuck.set(z, 1);
                                        carSpeed.set(z, 0);
                                    }
                                    break;
                                case 3:
                                    if(!carYPos.contains(y+speed)) carYPos.set(z, y+speed);
                                    else{
                                        carStuck.set(z, 1);
                                        carSpeed.set(z, 0);
                                    }
                                    break;
                                case 4:
                                    carXPos.set(z, x+speed);
                                    carYPos.set(z, y-speed);
                                    break;
                                case 5:
                                    carXPos.set(z, x-speed);
                                    carYPos.set(z, y-speed);
                                    break;
                                case 6:
                                    carXPos.set(z, x-speed);
                                    carYPos.set(z, y+speed);
                                    break;
                                case 7:
                                    carXPos.set(z, x+speed);
                                    carYPos.set(z, y+speed);
                                    break;
                                default: break;
                            }*/
                            switch(direction){
                                case 0:
                                    carXPos.set(z, x+speed);
                                    break;
                                case 1:
                                    carYPos.set(z, y-speed);
                                    break;
                                case 2:
                                    carXPos.set(z, x-speed);
                                    break;
                                case 3:
                                    carYPos.set(z, y+speed);
                                    break;
                                case 4:
                                    carXPos.set(z, x+speed);
                                    carYPos.set(z, y-speed);
                                    break;
                                case 5:
                                    carXPos.set(z, x-speed);
                                    carYPos.set(z, y-speed);
                                    break;
                                case 6:
                                    carXPos.set(z, x-speed);
                                    carYPos.set(z, y+speed);
                                    break;
                                case 7:
                                    carXPos.set(z, x+speed);
                                    carYPos.set(z, y+speed);
                                    break;
                                default: break;
                            }
                            x=carXPos.get(z);
                            y=carYPos.get(z);
                            for(int a=0; a<numInters.size(); a++){
                                place=26;
                                //if(intersectionX.get(a)<x+10 && intersectionX.get(a)>x-10 && intersectionY.get(a)<y+10 && intersectionY.get(a)>y-10){
                                if(intersectionX.get(a)==x && intersectionY.get(a)==y){
                                    int[] tempSigIntIds= new int[4];
                                    int tempNum=0, tempDir, lightState, totalStopped=0;
                                    for(int var=0; var<intId.size(); var++){
                                        if(intId.get(var)==interIds.get(a) && tempNum<4){
                                            tempSigIntIds[tempNum]=lightStates.get(var);
                                            tempNum++;
                                        }
                                    }
                                    tempDir=tempSigIntIds[carDir2.get(z)];
                                    lightState=tempSigIntIds[tempDir];
                                    if(lightState==0) carSpeed.set(z, 0);
                                    else carSpeed.set(z, 1);
                                    for(int b:carSpeed) if(b==0) totalStopped++;//causes cars to not follow traffic signals
                                    if(totalStopped>carSpeed.size()/2) for(int c=0; c<carSpeed.size(); c++) carSpeed.set(c, 1);
                                    //Log.e(intersectionX.get(a)+"", intersectionY.get(a)+"");
                                    place=27;
                                    numSegs = numInters.get(a);
                                    if(numSegs!=4){
                                        place=28;
                                        if(carDir2.get(z)==0){
                                            carDir2.set(z, 2);
                                            carXPos.set(z, x-20);
                                        }
                                        else if(carDir2.get(z)==1){
                                            carDir2.set(z, 3);
                                            carYPos.set(z, y+20);
                                        }
                                        else if(carDir2.get(z)==2){
                                            carDir2.set(z, 0);
                                            carXPos.set(z, x+20);
                                        }
                                        else if(carDir2.get(z)==3){
                                            carDir2.set(z, 1);
                                            carYPos.set(z, y-20);
                                        }
                                        place=30;
                                        /*for(int temp123=0; temp123<numInters.size(); temp123++){
                                            if(numInters.get(temp123)==4){
                                                carXPos.set(z, intersectionX.get(temp123));
                                                carYPos.set(z, intersectionY.get(temp123));
                                                break;
                                            }
                                        }*/
                                    }else if(numSegs==4) carDir2.set(z, (int)(Math.random()*4));
                                    break;
                                }
                            }
                            place=31;
                        }
                        drawView.postInvalidate();
                    }
                    clientSocket.close();
                }catch(Exception e){Log.e("DYNAMIC ERROR "+place, e.toString());}
            }
        });
        thread2.start();
        //Log.d("CAR GIVEN DIRECTION", carDir.toString());
        //Log.d("CAR ACTUAL DIRECTION", carDir2.toString());
        //Log.d("CAR ID", carIds.toString());
        //Log.d("CAR POSITION", carPos.toString());
        //Log.d("CAR SEGMENT", carSeg.toString());
        //Log.d("CAR SPEED", carSpeed.toString());
        //Log.e("CAR X POSITION", carXPos.toString());
        //Log.e("CAR Y POSITION", carYPos.toString());
        //Log.e("TEXT IN THREAD", curr2+"");
    }

    public class DrawView extends View {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        ArrayList<Integer> xIntersCoords, yIntersCoords;
        final int ROAD_WIDTH=20, SIGNAL_WIDTH=10, SIGNAL_HEIGHT=30, CAR_SIZE=ROAD_WIDTH/2;
        float tempWidth;
        int color;

        public DrawView(Context context, ArrayList<Integer> intersA, ArrayList<Integer> intersB, int c){
            super(context);
            xIntersCoords = intersA;
            yIntersCoords = intersB;
            paint.setColor(Color.BLACK);
            color = c;
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.scale(0.1f, 0.1f);
            for(int x=0; x<MainActivity.segId; x++){//draw roads
                int x1 = MainActivity.intersectionX.get(MainActivity.inter1.get(x));
                int y1 = MainActivity.intersectionY.get(MainActivity.inter1.get(x));
                int x2 = MainActivity.intersectionX.get(MainActivity.inter2.get(x));
                int y2 = MainActivity.intersectionY.get(MainActivity.inter2.get(x));
                tempWidth = paint.getStrokeWidth();
                paint.setColor(Color.DKGRAY);
                paint.setStrokeWidth(ROAD_WIDTH);
                canvas.drawLine(x1, y1, x2, y2, paint);
                paint.setStrokeWidth(tempWidth);
            }
            for(int x=0; x<xIntersCoords.size(); x++){//draw traffic signals
                int xValue = xIntersCoords.get(x), yValue = yIntersCoords.get(x);
                paint.setColor(Color.BLACK);
                if(numInters.get(x)==4){
                    canvas.drawRect(xValue-ROAD_WIDTH/2-SIGNAL_HEIGHT-5, yValue-ROAD_WIDTH/2-SIGNAL_WIDTH-5, xValue-ROAD_WIDTH/2-5, yValue-ROAD_WIDTH/2-5, paint);//top-left
                    canvas.drawRect(xValue+ROAD_WIDTH/2+5, yValue-ROAD_WIDTH/2-SIGNAL_HEIGHT-5, xValue+ROAD_WIDTH/2+SIGNAL_WIDTH+5, yValue-ROAD_WIDTH/2-5, paint);//top-right
                    canvas.drawRect(xValue-ROAD_WIDTH/2-SIGNAL_WIDTH-5, yValue+ROAD_WIDTH/2+5, xValue-ROAD_WIDTH/2-5, yValue+ROAD_WIDTH/2+SIGNAL_HEIGHT+5, paint);//bottom-left
                    canvas.drawRect(xValue+ROAD_WIDTH/2+5, yValue+ROAD_WIDTH/2+5, xValue+ROAD_WIDTH/2+SIGNAL_HEIGHT+5, yValue+ROAD_WIDTH/2+SIGNAL_WIDTH+5, paint);//bottom-right
                    paint.setColor(Color.RED);
                    canvas.drawPoint(xValue-ROAD_WIDTH/2-SIGNAL_WIDTH, yValue+ROAD_WIDTH/2+SIGNAL_HEIGHT, paint);
                }
            }
            for(int x=0; x<sigId.size(); x++){//update traffic lights
                if(sigId.get(x)!=-1){
                    int centerX=intersectionX.get(intId.get(x)), centerY=intersectionY.get(intId.get(x));
                    int segP1=0, segP2=0, offsetX=0, offsetY=0;
                    for(int y=0; y<segmentId.size(); y++){
                        if(segmentId.get(y).equals(lightSegId.get(x))){
                            segP1=inter1.get(y);
                            segP2=inter2.get(y);
                            break;
                        }
                    }
                    if(segP1==intId.get(x)){
                        offsetX=intersectionX.get(segP2);
                        offsetY=intersectionY.get(segP2);
                    }else if(segP2==intId.get(x)){
                        offsetX=intersectionX.get(segP1);
                        offsetY=intersectionY.get(segP1);
                    }
                    paint.setColor(Color.BLACK);
                    if(offsetX==centerX && offsetY<centerY){
                        canvas.drawRect(centerX-ROAD_WIDTH/2-SIGNAL_WIDTH-5, centerY+ROAD_WIDTH/2+5, centerX-ROAD_WIDTH/2-5, centerY+ROAD_WIDTH/2+SIGNAL_HEIGHT+5, paint);//bottom-left
                        paint.setStrokeWidth((int)(SIGNAL_WIDTH-(SIGNAL_WIDTH/10.0)*4));
                        if(lightStates.get(x)==0){
                            paint.setColor(Color.RED);
                            canvas.drawPoint(centerX-ROAD_WIDTH/2-SIGNAL_WIDTH, centerY+ROAD_WIDTH/2+SIGNAL_HEIGHT, paint);
                        }
                        else if(lightStates.get(x)==1){
                            paint.setColor(Color.YELLOW);
                            canvas.drawPoint(centerX-ROAD_WIDTH/2-SIGNAL_WIDTH, (float)(centerY+ROAD_WIDTH/2+SIGNAL_HEIGHT*(2.0/3)), paint);
                        }
                        else{
                            paint.setColor(Color.GREEN);
                            canvas.drawPoint(centerX-ROAD_WIDTH/2-SIGNAL_WIDTH, (float)(centerY+ROAD_WIDTH/2+SIGNAL_HEIGHT*(1.0/3)), paint);
                        }
                    }else if(offsetX<centerX && offsetY==centerY){
                        paint.setStrokeWidth((int)(SIGNAL_WIDTH-(SIGNAL_WIDTH/10.0)*4));
                        if(lightStates.get(x)==0){
                            paint.setColor(Color.RED);
                            canvas.drawPoint(centerX+ROAD_WIDTH/2+SIGNAL_HEIGHT, centerY+ROAD_WIDTH/2+SIGNAL_WIDTH, paint);
                        }
                        else if(lightStates.get(x)==1){
                            paint.setColor(Color.YELLOW);
                            canvas.drawPoint((float)(centerX+ROAD_WIDTH/2+SIGNAL_HEIGHT*(2.0/3)), centerY+ROAD_WIDTH/2+SIGNAL_WIDTH, paint);
                        }
                        else{
                            paint.setColor(Color.GREEN);
                            canvas.drawPoint((float)(centerX+ROAD_WIDTH/2+SIGNAL_HEIGHT*(1.0/3)), centerY+ROAD_WIDTH/2+SIGNAL_WIDTH, paint);
                        }
                    }else if(offsetX==centerX && offsetY>centerY){
                        paint.setStrokeWidth((int)(SIGNAL_WIDTH-(SIGNAL_WIDTH/10.0)*4));
                        if(lightStates.get(x)==0){
                            paint.setColor(Color.RED);
                            canvas.drawPoint(centerX+ROAD_WIDTH/2+5+SIGNAL_WIDTH/2, centerY-ROAD_WIDTH/2-SIGNAL_HEIGHT, paint);
                        }
                        else if(lightStates.get(x)==1){
                            paint.setColor(Color.YELLOW);
                            canvas.drawPoint(centerX+ROAD_WIDTH/2+5+SIGNAL_WIDTH/2, (float)(centerY-ROAD_WIDTH/2-SIGNAL_HEIGHT*(2.0/3)), paint);
                        }
                        else{
                            paint.setColor(Color.GREEN);
                            canvas.drawPoint(centerX+ROAD_WIDTH/2+5+SIGNAL_WIDTH/2, (float)(centerY-ROAD_WIDTH/2-SIGNAL_HEIGHT*(1.0/3)), paint);
                        }
                    }else if(offsetX>centerX && offsetY==centerY){
                        paint.setStrokeWidth((int)(SIGNAL_WIDTH-(SIGNAL_WIDTH/10.0)*4));
                        if(lightStates.get(x)==0){
                            paint.setColor(Color.RED);
                            canvas.drawPoint(centerX-ROAD_WIDTH/2-SIGNAL_HEIGHT, centerY-ROAD_WIDTH/2-SIGNAL_WIDTH, paint);
                        }
                        else if(lightStates.get(x)==1){
                            paint.setColor(Color.YELLOW);
                            canvas.drawPoint((float)(centerX-ROAD_WIDTH/2-SIGNAL_HEIGHT*(2.0/3)), centerY-ROAD_WIDTH/2-SIGNAL_WIDTH, paint);
                        }
                        else{
                            paint.setColor(Color.GREEN);
                            canvas.drawPoint((float)(centerX-ROAD_WIDTH/2-SIGNAL_HEIGHT*(1.0/3)), centerY-ROAD_WIDTH/2-SIGNAL_WIDTH, paint);
                        }
                    }
                }
            }
            canvas.save();
            paint.setStrokeWidth((int)(SIGNAL_WIDTH-(SIGNAL_WIDTH/10.0)*4));
            for(int x=0; x<carXPos.size(); x++){
                paint.setColor(carColor.get(x));
                canvas.drawPoint(carXPos.get(x), carYPos.get(x), paint);
            }
        }
    }
}